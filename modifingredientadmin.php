<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>modification ingredient admin</title>
</head>

<body>

    <?php

    include "navadmin.html";

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "erreur de connexion : " . $e->getMessage();
    }
    ?>
    <?php

    $recupNom = isset($_POST["nom"]) && !empty($_POST["nom"]) ? $_POST['nom'] : '';
    $recupDescription = isset($_POST["descriptions"]) && !empty($_POST["descriptions"]) ? $_POST['descriptions'] : '';
    $recupPhoto = isset($_FILES['photo']) ? 'pngs/' . microtime() . $_FILES['photo']['name'] : " ";
    $recupType = isset($_POST["types"]) && !empty($_POST["types"]) ? $_POST['types'] : '';
    $recupIdIngredient = isset($_GET['id']) ? $_GET['id'] : '';
    $submit = isset($_POST["submit"]);

                            try {
                                    $req = $pdo->prepare("SELECT * FROM ingredient WHERE $recupIdIngredient = id_ingredient");
                                    $req->execute();
                                    $results = $req->fetchAll();
                                    $stockIngredient = $results[0];
                                    

                                } catch (PDOException $e) {
                                    echo "Veuillez remplir les champs " . $e->getMessage();
                                }  

    ?>

    <h2 id="ajout"> Modifier : <?php echo $stockIngredient ['nom']?></h2>

    <form enctype="multipart/form-data" action="" method="post">

        <div id="ingredient_admin">
            <div class="nom_photo">
                <p>Nom</p> <input type="text" name="nom" value=" <?php echo $stockIngredient['nom'] ?>"><br>
                <p>Description</p><textarea id="descriptions" name="descriptions" rows="5" cols="33"><?php echo $stockIngredient['descriptions'] ?></textarea><br>
            </div>

            <div class="nom_photo">
                <p>Photo de l'ingredient</p><input type="file" name="photo" placeholder=""> <br>
                <p>Type</p>
                <select name="types">
                    <option value="1" <?php if ($stockIngredient['id_type'] == 1) {echo "selected";}?>>Fruit</option>
                    <option value="2" <?php if ($stockIngredient['id_type'] == 2) {echo "selected";}?>>Légume</option>
                    <option value="3" <?php if ($stockIngredient['id_type'] == 3) {echo "selected";}?>>Épice</option>
                </select>
            </div>
        </div>
        <input id="submit_ajout" type="submit" name="submit" id="sauvegarder" value="Sauvegarder">
    </form>

<?php

if (isset($_POST['submit'])) {
    if (isset($_POST['nom']) && !empty($_POST['nom']) && isset($_POST['descriptions']) && !empty($_POST['descriptions']) 
    && isset($_FILES['photo']) && !empty($_FILES['photo']) && isset($_POST['types']) && !empty($_POST['types'])) {
        try {
            $req = $pdo->prepare("UPDATE ingredient SET id_type=?, nom=?, descriptions=?, photo=? WHERE $recupIdIngredient = id_ingredient ");
            $req->execute([$recupType, $recupNom, $recupDescription, $recupPhoto]);
            header('Location: listeadmin.php');
        } catch (PDOException $e) {
            echo "Erreur insert into: " . $e->getMessage();
        }
    } else {
        echo '<p>Veuillez remplir tous les champs SVP</p>';
    }
}




?>
</body>

</html>