<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>creation recette admin</title>
</head>

<body>

    <?php

    include "navadmin.html";

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "erreur de connexion : " .$e->getMessage();
    }

    $recupIdRecette = isset($_GET['id']) ? $_GET['id']:'';

    try {
        $req = $pdo->prepare("SELECT *, recette.nom AS nom_recette, ingredient.nom AS nom_ingredient FROM ingredient_recette 
        INNER JOIN ingredient ON ingredient.id_ingredient = ingredient_recette.id_ingredient 
        INNER JOIN recette ON recette.id_recette = ingredient_recette.id_recette WHERE recette.id_recette=?");
        $req->execute([$recupIdRecette]);
        $results = $req->fetchAll();
        $stockIngredient = $results[0];

    } catch (PDOException $e) {
        echo "Erreur select: ".$e->getMessage();
    }
    ?>

<h2 id="ajout"><?php echo $stockIngredient['nom_recette']; ?></h2>

    <section>

        <h2>Les ingredients</h2>

        <?php

        $suppIngredient = isset($_GET["sup"]) ? $_GET['sup']:'';
        $recupIdIngredient = isset($_GET["id_ingredient"]) ? $_GET["id_ingredient"] :'';
        $recupIdRecette = isset($_GET['id']) ? $_GET['id'] : '';

        foreach ($results as $portion) {

        echo "<p> - ".$portion['nb_portion']." portions de ".$portion['nom_ingredient']. 
        "<a href='?sup=ok&id=".$portion['id_recette']."&id_ingredient=".$portion['id_ingredient']."'>X</a></p><br>";
        }

        if ($suppIngredient == 'ok') {
            try {
                $req = $pdo->prepare("DELETE FROM ingredient_recette WHERE id_ingredient=? && id_recette =? ");
                $req->execute([$recupIdIngredient,$recupIdRecette]);
                header("Location: creationadmin.php?id=".$stockIngredient['id_recette']);
            } catch (PDOException $e) {
                echo "Erreur suppression : " .$e->getMessage();
            }
        }


        $recupQuantite = isset($_POST["quantite"]) && !empty($_POST["quantite"]) ? $_POST['quantite']: '';
        $recupIngredient = isset($_POST["liste_ingredient"]) && !empty($_POST["liste_ingredient"]) ? $_POST['liste_ingredient']: '';

        ?>

        <form id="creation" method="post" action="">

            <p>Quantité : </p><br>

            <input type="number" name="quantite"><p> portions de</p><select name="liste_ingredient">

                <?php
                try {
                    $req = $pdo->prepare("SELECT nom,id_ingredient  FROM ingredient");
                    $req->execute();
                    $results = $req->fetchAll();

                    foreach ($results as $ingredient) {
                        echo "<option value='".$ingredient['id_ingredient']."'>".$ingredient['nom']."</option>";
                    }
                } catch (PDOException $e) {
                    echo "Erreur select: ".$e->getMessage();
                }
                ?>
            </select>
            <input class="submit" type="submit" name="submit" id="ajouter" value="Ajouter">

        </form>
<?php
        if (isset($_POST['submit'])) {
            if (isset($_POST['quantite']) && !empty($_POST['quantite']) && isset($_POST['liste_ingredient']) && !empty($_POST['liste_ingredient'])) {
                try {
                    $req = $pdo->prepare("INSERT INTO ingredient_recette (id_recette,id_ingredient,nb_portion) VALUES (?,?,?)");
                $req->execute([$recupIdRecette,$recupIngredient,$recupQuantite]);
                        header('Location: ');
            } catch (PDOException $e) {
                        echo "Vous ne pouvez pas ajouter de nouveau cet ingredient. Veuillez le supprimer préalablement.";
             }
                } else {
                         echo '<p>Veuillez remplir tous les champs SVP</p>';
                }
             }
?>
<a href="listeadmin.php">Valider la recette </a>

</section>

</body>

</html>