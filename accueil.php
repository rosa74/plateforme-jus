<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylevisiteur.css">
    <title>accueil visiteur</title>
</head>

<body>

    <?php

    include "navVisiteur.php";

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
        echo "erreur de connexion : " . $e->getMessage();
    } 
    
    ?>

    <p id="p_titre"> Vous êtes plutôt : <a href="triagerecettefruit.php">Jus de fruit / </a>
        <a href="triagerecettelegume.php"> Jus de legumes ?</a></p>


      <div id="photo_accueil">  
<?php
        try {
        $req = $pdo->prepare("SELECT nom, photo FROM recette ");
        $req->execute();
        $results = $req->fetchAll();
        foreach ($results as $recette) {
             echo "<a href='presentationrecette.php'><img src='".$recette['photo']."'><p>".$recette['nom']."</p></a>";
        }
        } catch (PDOException $e) {
    echo "erreur de connexion : " . $e->getMessage();
}

?>

    </div>
        <p id="p_pied_accueil"> Vous êtes prêts a partager vos idées créatives ? </p>

            <div id="a_soumettez"><a  href="creation.php">Soumettez vos recettes de jus !</a></div>