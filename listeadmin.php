<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>liste admin</title>
</head>

<body>

    <?php

    include "navadmin.html";

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "erreur de connexion : " . $e->getMessage();
    }
    ?>

    <div id="liste">

        <div>
            <h4>Ingredients <a href="ajoutingredientadmin.php">Ajouter</a> </h4>

            <?php
            try {
                $req = $pdo->prepare("SELECT nom,id_ingredient FROM ingredient");
                $req->execute();
                $results = $req->fetchAll();

                foreach ($results as $ingredient) {

                    echo "<p><a href='modifingredientadmin.php?id=".$ingredient['id_ingredient']."'>".$ingredient['nom']."</a></p>";
                }
            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }
            ?>
        </div>

        <div>
            <h4>Recettes</h4>
            <?php
            try {
                $req = $pdo->prepare("SELECT nom,id_recette FROM recette");
                $req->execute();
                $results = $req->fetchAll();

                foreach ($results as $recette) {

                    echo "<p><a href='modifrecetteadmin.php?id=".$recette['id_recette']."'>".$recette['nom']."</a></p>";
                }
            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }
            ?>
        </div>

    </div>
















</body>

</html>