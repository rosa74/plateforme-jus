<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Ajout ingredient</title>
</head>

<body>

    <?php

    include "navadmin.html";

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "erreur de connexion : " . $e->getMessage();
    }
    ?>

    <h2 id="ajout"> Ajouter un ingredient </h2>

    <form  enctype="multipart/form-data" action="" method="post">

        <div id="ingredient_admin">
            <div class="nom_photo">
                <p>Nom</p> <input type="text" name="nom" placeholder=""><br>
                <p>Description</p><textarea id="descriptions" name="descriptions" rows="5" cols="33"></textarea><br>
            </div>

            <div class="nom_photo">
                <p>Photo de l'ingredient</p><input type="file" name="photo" placeholder=""> <br>
                <p>Type</p>
                    <select name="types">
                        <option value="1">Fruit</option>
                        <option value="2">Légume</option>
                        <option value="3">Épice</option>
                    </select>
                 </div>
        </div>
        <input id="submit_ajout" type="submit" name="submit" id="sauvegarder" value="Sauvegarder">
    </form>

    <?php

    $recupNom = isset($_POST["nom"]) && !empty($_POST["nom"]) ? $_POST['nom'] : '';
    $recupDescription = isset($_POST["descriptions"]) && !empty($_POST["descriptions"]) ? $_POST['descriptions'] : '';
    $recupPhoto = isset($_FILES['photo']) ? 'pngs/' . microtime() . $_FILES['photo']['name'] : " ";
    $recupType = isset($_POST["types"]) && !empty($_POST["types"]) ? $_POST['types'] : '';

    $submit = isset($_POST["submit"]);


    if (isset($_POST['submit'])) {
        if (
            isset($_POST['nom']) && !empty($_POST['nom']) && isset($_POST['descriptions']) && !empty($_POST['descriptions'])
            && isset($_FILES['photo']) && !empty($_FILES['photo']) && isset($_POST['types']) && !empty($_POST['types'])
        ) {
            try {
                $req = $pdo->prepare("INSERT INTO ingredient (nom, descriptions, photo, id_type) VALUES (?,?,?,?)");
                $req->execute([$recupNom, $recupDescription, $recupPhoto, $recupType]);
                move_uploaded_file($_FILES['photo']['tmp_name'], $recupPhoto);
                header('Location: listeadmin.php');

            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }
        } else {
            echo '<p>Veuillez remplir tous les champs SVP</p>';
            
        }
    }



    ?>

</body>

</html>