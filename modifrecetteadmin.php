<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>modification recette admin</title>
</head>

<body>

    <?php

    include "navadmin.html";

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "erreur de connexion : " . $e->getMessage();
    }
    ?>
    <?php

    $recupNom = isset($_POST["nom"]) && !empty($_POST["nom"]) ? $_POST['nom'] : '';
    $recupDescription = isset($_POST["descriptions"]) && !empty($_POST["descriptions"]) ? $_POST['descriptions'] : '';
    $recupPhoto = isset($_FILES['photo']) ? 'pngs/' . microtime() . $_FILES['photo']['name'] : " ";
    $recupIdIngredient = isset($_GET['id']) ? $_GET['id'] : '';
    $submit = isset($_POST["submit"]);

    try {
        $req = $pdo->prepare("SELECT * FROM recette WHERE $recupIdIngredient = id_recette");
        $req->execute();
        $results = $req->fetchAll();
        $stockIngredient = $results[0];
    } catch (PDOException $e) {
        echo "Erreur insert into: " . $e->getMessage();
    }

    ?>

    <h2 id="ajout"> Modifier : <?php echo $stockIngredient['nom'] ?></h2>

    <form enctype="multipart/form-data" action="" method="post">

        <div id="ingredient_admin">
            <div class="nom_photo">
                <p>Nom</p> <input type="text" name="nom" value=" <?php echo $stockIngredient['nom'] ?>"><br>
                <p>Description</p><textarea id="descriptions" name="descriptions" rows="5" cols="33"><?php echo $stockIngredient['descriptions'] ?></textarea><br>
            </div>

            <div class="nom_photo">
                <p>Photo de l'ingredient</p><input type="file" name="photo" placeholder=""> <br>
            </div>
        </div>
        <input id="submit_ajout" type="submit" name="submit" id="sauvegarder" value="Éditer les ingredients">
    </form>

    <?php

    if (isset($_POST['submit'])) {
        if (
            isset($_POST['nom']) && !empty($_POST['nom']) && isset($_POST['descriptions']) && !empty($_POST['descriptions'])
            && isset($_FILES['photo']) && !empty($_FILES['photo']) 
        ) {
            try {
                $req = $pdo->prepare("UPDATE recette SET nom=?, descriptions=?, photo=? WHERE $recupIdIngredient = id_recette ");
                $req->execute([$recupNom, $recupDescription, $recupPhoto]);

                header("Location: creationadmin.php?id=" .$stockIngredient['id_recette']);

            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }
        } else {
            echo '<p>Veuillez remplir tous les champs SVP</p>';
        }
    }
    
    
    ?>
</body>

</html>