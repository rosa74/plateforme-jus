<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>connexion admin</title>
</head>

<body>

    <?php
    include "navadmin.html"
    ?>

    <div id="formulaire">

        <p> Connexion </p>

        <form id="connexion" method="get" action="">

            <p>Identifiant</p> <input type="text" name="identifiant" placeholder=""><br>
            <p>Mot de passe</p><input type="password" name="mdp" placeholder=""> <br>
            <input class="submit" type="submit" name="submit" id= "envoyer" value="Connexion">
        </form>

    </div>


    <?php

    $servname = "localhost";
    $dbname = "recette_jus";
    $user = "admin";
    $pass = "mdp";

    try {
        $pdo = new PDO("mysql:host=$servname;dbname=$dbname;", $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "erreur de connexion : " . $e->getMessage();
    }

$recupIdentifiant = isset($_GET['identifiant']) && !empty($_GET['identifiant']) ? $_GET['identifiant'] : "";
$recupMdp = isset($_GET['mdp']) && !empty($_GET['mdp']) ? $_GET['mdp'] : "";

if (isset($_GET['submit'])) {
    if (isset($_GET['identifiant']) && !empty($_GET['identifiant']) && isset($_GET['mdp']) && !empty($_GET['mdp'])) {
        try {
            $req = $pdo->prepare("SELECT * FROM administrateur");
            $req->execute();
            $results = $req->fetchAll();
            $identifiantValid = null;

            foreach ($results as $admin) {
                if ($recupIdentifiant == $admin["identifiant"] && $recupMdp == $admin["mdp"]) {
                    header("Location: listeadmin.php");
                    $identifiantValid = true;
                }
            }
            if ($identifiantValid != true) {
                echo "<p class = 'Erreur'>L'identifiant ou le mot de passe n'est pas valide</p>";
            }
        } catch (PDOException $e) {
            echo "Erreur insert into: " . $e->getMessage();
        }
    } else {
        echo '<p class = "Erreur">Veuillez remplir tous les champs SVP</p>';
    }
}



    ?>
</body>

</html>